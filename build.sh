#!/bin/sh -x

dn=$(dirname $0)
if [ "${dn}" = "." ]; then
  echo "Run the build in a subdirectory or in an out-of-source directory"
  exit 1
fi

topdir=$(readlink -f ${dn})
builddir=$(readlink -f .)
papersdir=${builddir}/papers
latex_templates=${topdir}/instructions/LaTeXTemplates.zip

die() {
    echo >&2 "$1"
    exit 1
}

build_from_source() {
    local project project_dir main_tex_file other_files bib_file ok
    project_dir="$1"
    project=$(echo ${project_dir} | tr '/' '_')
    main_tex_file="$2"
    [ -f "${topdir}/contributions/${project_dir}/${main_tex_file}" ] || die "invalid call to build_from_source()"
    shift; shift;
    other_files="$@"

    if [ ! -d ${project_dir} ]; then
	mkdir -p ${project_dir}
	cd ${project_dir}

	cp ${topdir}/contributions/${project_dir}/${main_tex_file} ./${project}.tex \
	    && for f in ${other_files}; do \
	           cp -a ${topdir}/contributions/${project_dir}/${f} .; done \
	    && unzip ${latex_templates} \
	    && pdflatex ${project} \
	    && if grep bibdata ${project}.aux; then
	           unzip -j BibTeX/iopart-num.zip iopart-num/iopart-num.bst
		   bibtex ${project}
	       fi \
	    && pdflatex ${project} \
	    && ln -sf ${builddir}/${project_dir}/${project}.pdf ${papersdir}

	cd -
    fi
}

link_pdf() {
    local project project_dir pdf_file
    project_dir="$1"
    project=$(echo ${project_dir} | tr '/' '_')
    pdf_file="$2"
    [ -f "${topdir}/contributions/${project_dir}/${pdf_file}" ] || die "invalid call to link_pdf()"

    ln -s "${topdir}/contributions/${project_dir}/${pdf_file}" ${papersdir}/${project}.pdf
}

if [ ! -d ${papersdir} ]; then
    mkdir -p ${papersdir}
fi

#ln -sf ${topdir}/contributions/user_support/ar.pdf       ${papersdir}/user_support.pdf

cd ${builddir}

# build cover
# if [ ! -d cover ]; then
#     mkdir cover && cd cover
# 
#     convert -verbose ${topdir}/contributions/cover/ar_ok-01.tif cover.pdf
#     ln -s ${builddir}/cover/cover.pdf ${papersdir}/cover.pdf
# 
#     cd ..
# fi

link_pdf          alice        "ALICE per Annual Report CNAF formatted 30.4.2015.pdf"
build_from_source ams          2014_AMS_CNAF.tex AMS_2014.pdf AMSprod.pdf AMSshort.pdf contributors.pdf data_flow_2.pdf dataitaly.pdf
link_pdf          atlas        ATLAS-report-CNAF-201503.pdf
build_from_source auger        AugerARperCNAF_2.tex Exposure.eps
link_pdf          belle2       CNAFBelle2-2014.pdf
build_from_source borexino     Borexino_CNAFreport2014.tex
build_from_source cdf          CDF_CNAF_annual_report.tex cdfbib.bib figures
link_pdf          chaos        chaosAnnualReportCNAF2014.pdf
build_from_source cloud_cnaf   cloud_cnaf.tex jenkins.png storm.png
link_pdf          cms          REPORT_CMS_CNAF_2014.pdf
build_from_source coka         report-coka-v6.tex fig
build_from_source cta          CTA_AnnualReport_2015.tex
build_from_source cuore        cnaf_cuore.tex cnaf_cuore.bib
build_from_source eee          consCNAFeee.tex EEEarch.eps EEEmonitor.eps EEEpilotrun.eps
build_from_source gerda        gerda_cnaf.tex
build_from_source glast        cnaf_2014_v3.tex jobs.png share.png time.png
build_from_source km3tridas    km3-tridas.tex schema.jpg test.jpg
link_pdf          lhcb         LHCbCNAFAnnualReport2015.pdf
build_from_source lhcb_eb      LHCb.EB.Antonio.Falabella.tex *.eps
link_pdf          na62         na62@cnaf.pdf
build_from_source opera        CNAF_report_Opera2014.tex Fig1_new2.pdf OperaSoftSchema_v3.pdf
build_from_source t1/farming   AnnualReportFarming.tex images
build_from_source t1/network   t1_network.tex netboard.png netlab.png wan.png
build_from_source user_support ar.tex cpu.eps disk_lhc.eps disk_nonlhc.eps tape.eps
build_from_source virgo        VirgoatCNAF.tex BS.png SCHEMA.png
build_from_source wnodes       ReportWNoDeS2.tex
build_from_source xenon        report_CNAF.tex

pdflatex ${topdir}/ar.tex \
&& pdflatex ${topdir}/ar.tex 2> /dev/null \
&& pdflatex ${topdir}/ar.tex 2> /dev/null
