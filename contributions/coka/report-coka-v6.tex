\documentclass[a4paper]{jpconf}

\usepackage{bm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{eurosym}

%\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{url}
\usepackage{listings}

\definecolor{g90}{gray}{.90}
\definecolor{r1}{rgb}{.75, .10, .10}
\definecolor{g1}{rgb}{.10,.75,.10}
\definecolor{b1}{rgb}{.10, .10, .75}
\definecolor{y1}{rgb}{.85, .50, .30}

\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{CadetBlue}{rgb}{0.28,0.23,0.54}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{r2}{rgb}{.80, .10, .10}
\definecolor{r3}{rgb}{.60, .10, .10}

\lstset{
    language=C, 
%    backgroundcolor=\color{g90},
    rulecolor=\color{red},
    frame=single, 
%    frame=ltrb, 
%    frame=trBL, 
    basicstyle=\small,
    keywordstyle=\ttfamily\color{OliveGreen}\bfseries\underbar,
    morekeywords={uint}, 
    identifierstyle=\ttfamily\color{CadetBlue}\bfseries, 
    commentstyle=\color{Brown},
    stringstyle=\ttfamily,
%    basewidth={0.4em,0.225em},
    fontadjust=true,
    showstringspaces=false
}

\lstdefinelanguage{myC}[]{C}{
  morekeywords={uint, REUSE, signal, wait, offload, offload_transfer, offload_wait, in, out, into}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\bc}{\begin{center}}
\newcommand{\ec}{\end{center}}

\newcommand{\bd}{\begin{description}}
\newcommand{\ed}{\end{description}}

\newcommand{\bit}{\begin{itemize}}
\newcommand{\eit}{\end{itemize}}

\newcommand{\be}{\begin{enumerate}}
\newcommand{\ee}{\end{enumerate}}

\newcommand{\bq}{\begin{quote}}
\newcommand{\eq}{\end{quote}}

\newcommand{\un}{\underline}

\newcommand{\hh}{\hspace*{.44cm}} % two   spaces
\newcommand{\hu}{\hspace*{.66cm}} % three spaces

\newcommand{\mynote}[1]{ \colorbox{yellow}{\tt \color{red}\underline{NOTE}: #1} }

%\parindent 0cm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{
The COKA Project 
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\author{
  R Alfieri$^1$,
  M Brambilla$^1$,
  E Calore$^2$,
  R De Pietri$^1$,
  F Di Renzo$^1$,\\
  A Feo$^1$,
  S F Schifano$^2$ and 
  R Tripiccione$^2$
}
  
\address{$^1$ Universit\`a di Parma and INFN-Gruppo collegato di Parma, ITALY}
\address{$^2$ Universit\`a di Ferrara and INFN-Ferrara, ITALY}

\ead{
  alfieri@pr.infn.it,
  brambilla@pr.infn.it,
  calore@fe.infn.it,
  depietri@pr.infn.it,
  direnzo@pr.infn.it,
  feo@pr.infn.it,
  schifano@fe.infn.it,
  tripiccione@fe.infn.it,
}
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract}

This document describes the work carried out by the 
COKA project in 2014.
%
In summary, the project has focused on the use of
MIC-based accelerator boards as working horses for several computational applications in theoretical physics.  

These accelerators were released by Intel at the beginning of
2013 under the name Xeon-Phi. We have worked on the development of the best optimization strategies to use these processor efficiently, including an analysis of the available programming environments, and have assessed the performance and usability of these systems with respect to more standard processors. We have also started to consider the best options to use parallel systems containing several MIC accelerators. 
%


\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

The {\em COmputing on Knights Architectures} (COKA) project
started in 2012 with the goal of testing the Intel Many Integrated Core (MIC) 
architecture for applications relevant for theoretical and experimental physics, 
assessing its performance and efficiency. 
%
In 2013 we started to work with the first production release of a MIC board,
called Xeon-Phi.

A large fraction of our work has been focused on computational physics: 
we have completed the porting of a production-grade lattice-Boltzmann code 
for computational fluid-dynamics, and investigated scalability over a 
multi-accelerator system.
%
We have also tested performance of two widely used application framework, 
Einstein Toolkit and CHROMA, and investigated programming methodologies 
that allow to easily port codes across accelerators.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Main Results}

In the following we highlight some interesting results obtained during the 
year 2014.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Experimental Hardware}

We have used the funds assigned to the project to procure, install and
configure  part of an R\&D testbed at CNAF. 
%
The COKA part of this testbed currently contains two servers, with a 
total of 3 Intel Xeon-Phi boards, 2 NVIDIA K20 boards and 24 x86-cores processors.
%
These machines are accessible by COKA users through a user interface and a 
batch system, configured with a number of queues serving different 
purposes. 
%
For part of our work (mainly for the analysis of the scaling properties of our systems  on massively parallel systems)
we have also used accelerator based systems installed at CINECA 
and at the J\"ulich Supercomputer Centre.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Computational Fluid-dynamics}

We have completed the porting of our fluid-dynamics code based on Lattice 
Boltzmann methods on the  Xeon-Phi architecture in 2013~\cite{iccs13}.  
%
In 2014 we have fine-tuned the code to run it efficiently on 
multi-accelerator systems; we have also compared the performance of the same physics code, optimized for several architectures, including
GPU accelerators~\cite{PPAM11,PARCFD11,INPAR12} 
and ``classic'' multi-core CPUs~\cite{ICCS11,CCP12,PARCFD12}.

On multi-accelerator systems, accelerators are installed 
on the same host (typically up to 4 or 8 accelerators) or on multiple 
hosts that exchange data using commodity network such as Infiniband. 
%
Here we discuss an implementation for one host attached to
four accelerators.

The computationally relevant core of a Lattice Boltzmann program is basically a stencil code working on a discrete and regular spatial lattice. Two routines are associated to most of the computational load: {\sf propagate} is dominated by memory moves at regular but sparse memory addresses, while {\sf collide} applies a large set of mathematical operations to the data items associated to each lattice site; finally {\sf pbc} contains all the communications steps across different processing elements.

Our implementation splits a lattice of size $L_x \times L_y$ on $N_p$ 
accelerators along the $X$ dimension, each handling a {\em sub-lattice} 
of size $ L_x / N_p \times L_y$.
%
This splitting is necessary to have continuous halo-columns allocated 
in memory, and avoid a further gather-step to collect halos on a 
contiguous buffer before doing communications. 
%
This allocation scheme implies a virtual ordering of the accelerators 
along a ring, so each one is connected with a previous and a next 
companion; at the beginning of each time-step, before starting 
{\tt propagate}, accelerators must exchange data, since cells close 
to the right and left edges of the sub-lattice needs data allocated 
on the logically previous and next nodes.

When using multi-accelerator systems, the overlap of communications 
with computation is a key approach to scalability. 
%
In our case, the key point to consider is that {\sf propagate} 
for the bulk of the lattice (all lattice points except for three columns 
at right and left) has no data dependency with {\sf pbc} (while {\sf propagate} 
on the edges depend on fresh data moved to the halos by {\sf pbc}). 
%
Our strategy therefore tries to overlap as much as possible data 
transfers with the execution of {\sf propagate} on the bulk. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{fig/execution_flow}
\caption{\label{execution-flow} Program schedule allowing to overlap
communications and processing of the {\tt propagate} kernel. 
}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The details of the program schedule that optimize this overlap are shown in
Figure~\ref{execution-flow}. The overall sequence of operations is the same for our MIC and GPU implementations.
%
%We have scheduled operations as shown in figure~\ref{execution-flow}:
%
%i)   the host launches propagate on the bulk of the lattice; this is an 
%     asynchronous kernel which run in parallel with the following steps;
%ii)  the host starts the copy of the left and right column borders 
%     from device to host ({\tt D2H});
%iii) as {\tt D2H} finishes the host performs the corresponding MPI 
%     communications to exchange data with neighbor nodes;
%iv)  as MPI receive operations are completed data are moved back on the %accelerator ({\tt H2D});
%v)   finally {\tt propagate} executes on the left and right borders.
%%
%When all the above steps are completed execution continues with {\tt bc} 
%and {\tt collide} kernels. This schedule is the same for our MIC and GPU 
%implementations.
%
A conceptually irrelevant but technically important difference between the two cases is that on MIC systems MPI functions cannot access data allocated on the accelerator, 
so the code must explicitly move data between host and accelerator. This require additional care: details are shown in
\figurename~\ref{mic-code}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}[t]
\centering
\begin{lstlisting}[basicstyle=\tiny,language=myC]
// launch asynchronous transfer from device to host (D2H)
#pragma offload_transfer: out( cf2[LEFT_HALO]  : REUSE into( send_L_buf ) ) signal( &send_L_buf )
#pragma offload_transfer: out( cf2[RIGHT_HALO] : REUSE into( send_R_buf ) ) signal( &send_R_buf )

// launch asynchronous execution of propagate kernel over BULK
#pragma offload: signal( &internal_prop_signal ){ propagate_m ( ... ); }

// wait end of d2h transfer
#pragma offload_wait: wait( &send_L_buf )
#pragma offload_wait: wait( &send_R_buf )

// execute halos SWAP
MPI_Sendrecv(send_R_buf to mpi_rank_R, TAG_RIGHT, recv_L_buf to mpi_rank_L, TAG_RIGHT);
MPI_Sendrecv(send_L_buf to mpi_rank_L, TAG_LEFT,  recv_R_buf to mpi_rank_R, TAG_LEFT);

// launch asynchrouns transfer from host to device (H2D)
#pragma offload_transfer: in( recv_L_buf : REUSE into(cf2[LEFT_HALO] )) signal( &recv_L_buf )
#pragma offload_transfer: in( recv_R_buf : REUSE into(cf2[RIGHT_HALO])) signal( &recv_R_buf )

// wait end of h2d transfer
#pragma offload_wait: wait( &recv_L_buf )
#pragma offload_wait: wait( &recv_R_buf )

// launch asynchronous execution of propagate over left- and right-columns 
#pragma offload { propagate_m ( ... ); } signal(&external_prop_signal)

// wait end of propagate kernels
#pragma offload_wait: wait( &internal_prop_signal )
#pragma offload_wait: wait( &external_prop_signal )
\end{lstlisting}
\caption{\label{mic-code} Scheduling of operations on the MIC board to 
overlap communications and execution of {\tt propagate}.}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}[b]
\centering
\caption{\label{tab:results} 
Performance comparison of the {\tt propagate} and {\tt collide} kernels on
KNC and Kepler accelerators and on a dual 8-core E5-2630 (Intel Haswell 
micro-architecture) processor running at 2.4~GHz.\vspace*{5mm}
}
\resizebox{\textwidth}{!}{
\begin{tabular}{||l|r|r|r|r||r|r|r|r||r|r||}
\hline
                            & \multicolumn{4}{|c||}{Intel Xeon 7120} & \multicolumn{4}{|c||}{NVIDIA K80} & \multicolumn{2}{|c||}{E5-2630 v3} \\
\hline                      
% Peak Bw                               352                               240x2                              59
% Peak GFlops                          1208                              1455x2                             307
% Lattice Size                          5760 x 4096                  &    4800 x 4096                    & 4800 x 4096
\# devices(s)               &   1    &   2     &  3      &  4        & 1    & 2      & 3 & 4             & 1      & 2     \\
\hline
%$T_{\mbox{prop}}$ (msec)    & 164.3 &  86.6   &  62.1   &  51.0     &      &        &   &               & 292.6  & 133   \\
%$T_{\mbox{bc}}$   (msec)    &   6.6 &   5.1   &   4.9   &   5.4     &      &        &   &               & 1.8    & 1.7   \\
%$T_{\mbox{col}}$  (msec)    & 435.2 & 219.9   & 147.7   & 112.8     &      &        &   &               & 1359.4 & 679.8 \\
%$T_{\mbox{tot}}$  (msec)    & 606.1 & 311.9   & 215.1   & 169.4     &      &        &   &               & 1676.6 & 843.8 \\            \\
\hline
Propagate (GB/s)            & 85     & 161     & 225     & 274       & 154   & 266    &   393 &  520     & 40     & 88    \\
$S_r$                       & 1.0X   & 1.90X   & 2.65X   & 3.22X     & 1.0X  & 1.72X  & 2.55X & 3.37X    & 1.0X   & 2.2X  \\
\hline
Collide (GFs)               & 358    & 709     & 1056    & 1383      &  667  & 1359   &  2029 & 2706     & 111    & 222   \\
$S_r$                       & 1.0X   & 1.98X   & 2.95X   & 3.86X     & 1.0X  & 2.03X  & 3.07X & 4.05X    & 1.0X   & 2.0X  \\
\hline\hline
%Global P (GF/s)             & 257    &  500    & 725     &  920      & 468   & 907    & 1351  & 1796     &  90    & 178   \\
Global P (MLUPS)            & 39     &   73    & 110     &  139      &  72   & 140    & 209   & 277      & 12     & 23    \\
$S_r$                       & 1.0X   & 1.95X   & 2.82X   & 3.56X     & 1.0X  & 1.93X  & 2.88X & 3.84X    & 1.0X   & 1.98X \\
\hline
\end{tabular}
}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\centering
\begin{minipage}{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{fig/chroma}
\end{minipage}
%                                                                                            
\begin{minipage}{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{fig/etk}
\end{minipage}
\centering
\begin{minipage}{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{fig/etk_MPI.pdf}
\end{minipage}
%                                                                                            
\begin{minipage}{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{fig/etk_OMP.pdf}
\end{minipage}
\caption{
The top-left panel shows the time needed to perform
200 Monte Carlo sweeps on a $12^3\times 20$ lattice for pure
Gauge SU(3) using CHROMA. The execution time is 341 seconds on a host core
while it is 7033 seconds on a PHI core.  The top-right panel shows
the time needed to compute 32 time-evolution steps of
a single General Relativistic Star using the EinsteinToolkit on a $65^3$
grid (0.6 total GBytes allocated memory). On a single
host core the requested time is 410 seconds while on a single PHI core the
requested time is 6857 seconds. The bottom panels disaggregate 
MPI (left) and OpenMP (right) scaling data for the  EinsteinToolkit.
\label{chroma-etk}}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In \tablename~\ref{tab:results} we compare performance figures of our 
code optimized for the Xeon Phi, the Kepler K80 GPU and for a dual-processor 
commodity systems~\cite{PARCFD12} (dual Intel E5-2630), based on the 
Haswell micro-architecture.

We first focus on comparing performances with just one accelerator.
%
The {\tt propagate} kernel is a memory-bound step corresponding to
memory copies with sparse address patterns. On the {\em Kepler}  
architecture we reach $\approx 64\%$ of the available peak bandwidth.
% 
The {\em Xeon-Phi}, that uses the same class of memories, obtains a lower bandwidth, 
$\approx 85$ GB/s, that is $\approx 24\%$ of peak. This is mainly due to the
limited bandwidth ($\approx 220$ GB/s) of the internal ring, connecting cores
and memory controllers.
%
On the Haswell processor we measure $\approx 40$ GB/s corresponding to 
$\approx 67\%$ of the available peak.

The {\tt collide} kernel is a strongly compute-bound step, requiring 
approximately 20 double-precision floating-point operations per byte. 
%
On {\em Kepler}, this kernel reaches a sustained performance of 
$\approx 46\%$ of the available peak. 
%
The {\em Xeon-Phi} performance is lower, only approximately $\approx 30\%$ of 
the available peak, while on the Haswell CPU we measure an efficiency of 
$\approx 29\%$.  
%
The last section (Global P) of the table shows the performance of the full 
code, measured in {\em Millions Lattice Update Per Second} (MLUPS).
Comparing with the traditional eight-core CPU, the {\em Xeon-Phi} is 
$\approx 3$X faster, while on one GPU of the K80 system, the speed-up is 
$6$X.

\tablename~\ref{tab:results} also shows scalability results ($S_r$).
%
We see that individual steps and the full code scale quite well meaning that 
communications have been successfully hidden with computation; running with 4 GPUs the sustained performance of the full code is $\approx 1.7$ TFLOPS.

In conclusion, our application enjoys up to a $6$X performance 
increase running on accelerators, and also a good scaling on 
multi-accelerator systems.
% 
While these are valuable results, we underline that they were obtained 
with handcrafted optimizations tailored for each target accelerator.
% 
This is mainly due to the lack of programing methodologies able to support 
execution of the same code on different accelerator architectures.
%
Portability of codes and also of performances is today a real issue 
that needs to be solved to make accelerators a standard solution 
for HPC computing. 


\begin{figure}
\centering
\centering
\includegraphics[width=\textwidth]{fig/overlap}
\caption{Communication-computation overlap between host system and GPU 
implemented.\label{fig:operlap}}
\end{figure}



\subsection{Chroma and Cactus}

We started a test work on two widely used and freely available
application frameworks: the \textbf{CHROMA} lattice QCD application
\cite{CHROMA} and the \textbf{Einstein Toolkit\ } Astrophysical
Application suite \cite{ETK}. The main problem we had to deal with was
the compilation (for native mode execution) of the auxiliary libraries
needed by these applications. The strategy used in testing these
applications was to analyze the performance that can be obtained by
just recompiling on the MIC environment without any code
customizations, that is, using the potential key advantage of the MIC
architecture with respect to other accelerators like GPUs. The key
result of this simple procedure is that it is indeed possible --
albeit with a non negligible effort -- to run an unmodified large
scientific package on the MIC, but performances are dramatically
unsatisfactory if compared with the execution on the host (2x Sandy
Bridge, E5-2687W 3.10 GHz, 8 cores) so a significant tuning and
optimization work is definitely necessary.  The results of porting are
summarized in Figure~\ref{chroma-etk} where in the top panels are
expressed the overall scaling result (the dashed lines represent
perfect scaling) while the bottom panels show the additional problems
of the OpenMP bad scaling of the use of a standard OpenMP porting when
the number of threads is large.


\subsection{Test of portable programming techniques}

A further contribution to the project has been the study of portable
programing techniques that can be exploited on GPUs on many-core
systems. This work was carried out in the framework of a Thesis for
the ``laurea'' degree in Physics and was based on the porting of the
classical ``game-of-life'' with an addition of a highly vectorizable
computational core. The programming environments used were OpenMP for
the {\it Xeon-Phi} (MIC) and openACC for the {\it Kepler} GPU, the
common idea in both cases was to use ``annotated'' standard C code.
On the GPU side the possibility to overlap communication (GPU-host)
and computation (see Figure~\ref{fig:operlap}) is extremely
relevant. Using this technique a {\it Kepler} GPU has a speedup of
77.5 with respect to a serial version of the same code on a dual
8-core E5-2630 Xeon processor.  For the {\it Xeon-Phi} (MIC) the
speedup is much smaller, 19.1, using the maximum allowed number of
threads (240).  To explain this difference, one remarks that on the
{\it Kepler} GPU it was essential to overlap computation and memory
access and to ensure a reasonable level of vectorization of the main
computational kernel; these were not possible using the OpenMP 2.0
directives used for the {\it Xeon-Phi} porting.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{Acknowledgments}
{\small
This work was done in the framework of the COKA, and SUMA projects 
of INFN. We thank CINECA (Bologna, Italy), and the NVIDIA J\"ulich Application 
Lab (J\"ulich Supercomputer Center, Germany) for allowing us to use their 
computing systems.
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{References}

\begin{thebibliography}{99}

\bibitem{iccs13} 
G. Crimi, F. Mantovani, M. Pivanti, S.F. Schifano, R. Tripiccione, 
{\em Early Experience on Porting and Running a Lattice Boltzmann Code on the Xeon-Phi Co-Processor}, 
Proceedings of the International Conference on Computational Science, 
ICCS 2013 Procedia Computer Science, Volume 18, 2013, Pages 551-560, 
doi:10.1016/j.procs.2013.05.219.

\bibitem{PPAM11}
L. Biferale, F. Mantovani, M. Pivanti, F. Pozzati, M. Sbragaglia, A. Scagliarini, 
S. F. Schifano, F. Toschi, R. Tripiccione, 
{\em A multi-GPU implementation of a D2Q37 Lattice Boltzmann Code}, 
9a International Conference on Parallel Processing and Applied Mathematics (PPAM11),
September 11-14, 2011, Torun (Poland).
R. Wyrzykowski et al. (Eds.): PPAM 2011, Part I, LNCS 7203, pp. 640-650. Springer, Heidelberg (2012), 
doi:10.1007/978-3-642-31464-3\_65.

\bibitem{PARCFD11}
L. Biferale, F. Mantovani, M. Pivanti, F. Pozzati, M. Sbragaglia, A. Scagliarini, 
S. F. Schifano, F. Toschi, R. Tripiccione, 
{\em An Optimized D2Q37 Lattice Boltzmann Code on GP-GPUs}
Proceedings of 23rd International Conference on Parallel Computation Fluid Dynamics (PARCFD)
May 16-20 Barcelona (Spain), 
Computers and Fluids Vol. 80 (2013), pp. 55-62, doi:10.1016/j.compfluid.2012.06.003. 

\bibitem{INPAR12}
A. Bertazzo, F. Mantovani, M. Pivanti, F. Pozzati, S.F. Schifano, R. Tripiccione, 
{\em Implementation and Optimization of a Thermal Lattice Boltzmann Algorithm on a multi-GPU cluster}, 
Proceedings of Innovative Parallel Computing (INPAR) 2012,
May 13-14, 2012 San Jose, CA (USA), doi:10.1109/InPar.2012.6339603.


\bibitem{ICCS11}
L. Biferale, F. Mantovani, M. Pivanti, F. Pozzati, M. Sbragaglia, A. Scagliarini, 
S. F. Schifano, F. Toschi, R. Tripiccione, 
{\em Optimization of Multi-Phase Compressible Lattice Boltzmann Codes on Massively Parallel Multi-Core Systems}, 
International Conference on Computational Science (ICCS), June 1-3, 2011, Singapore
Procedia Science Vol. 4, pp. 994-1003, 2011, 
doi:10.1016/j.procs.2011.04.105.

\bibitem{CCP12}
F. Mantovani, M. Pivanti, S.F. Schifano, R. Tripiccione, 
{\em Exploiting parallelism in many-core architectures: a test case based on Lattice Boltzmann Models}, 
Proc. of Conference on Computational Physics October 14-18, 2012 Kobe, Japan, 
J. Phys. Conf. Ser. 454 Vol. 1, 2013, doi:10.1088/1742-6596/454/1/012015
 
\bibitem{PARCFD12}
F. Mantovani, M. Pivanti, S.F. Schifano, R. Tripiccione, 
{\em Performance issues on many-core processors: A D2Q37 Lattice Boltzmann scheme as a test-case}, 
Proceedings of 24rd International Conference on Parallel Computation Fluid Dynamics (PARCFD), 
May 21-25, 2012, Atlanta, GE (USA), Computers and Fluids Volume 88, 15 December 2013, Pages 743-752 (2013), 
doi: 10.1016/j.compfluid.2013.05.014.

\bibitem{CHROMA}
R.~G.~Edwards {\it et al.}  [SciDAC and LHPC and UKQCD Collaborations],
{\em The Chroma software system for lattice QCD},
Nucl.\ Phys.\ Proc.\ Suppl.\  {\bf 140} (2005) 832

\bibitem{ETK}
Frank L\"offler, Joshua Faber, Eloisa Bentivegna, Tanja Bode, Peter Diener, Roland Haas, Ian\
 Hinder,
 Bruno C. Mundim, Christian D. Ott, Erik Schnetter, Gabrielle Allen, Manuela Campanelli, and\
 Pablo Laguna.
 The Einstein Toolkit: A Community Computational Infrastructure for Relativistic Astrophysic\
s.
 Classical and Quantum Gravity, 29(11):115001, 2012. (doi:10.1088/0264-9381/29/11/115001)

\end{thebibliography}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}
