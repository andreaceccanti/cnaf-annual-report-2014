\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
%\documentclass{letter}
\usepackage{xspace}
\newcommand{\Fermi}{\emph{Fermi}\xspace}
\begin{document}
\title{The \Fermi-LAT experiment at the INFN CNAF Tier 1}

\author{
%L Arrabito$^{1}$,
%J Bregeon$^{1}$,
%J Cohen-Tanugi$^{1}$,
M Kuss$^{1}$,
F Longo$^{2}$,
%F Piron$^{1}$,
S Viscapi$^{3}$
and S Zimmer$^{4}$,
on behalf of the \Fermi LAT collaboration}

\address{$^{1}$ Istituto Nazionale di Fisica Nucleare, Sezione di Pisa, I-56127 Pisa, Italy}
\address{$^{2}$ Department of Physics, University of Trieste, via Valerio 2, Trieste and INFN, Sezione di Trieste, via Valerio 2, Trieste, Italy}
\address{$^{3}$ Laboratoire Univers et Particules, Universit\'{e} de Montpellier II Place Eug\`{e}ne Bataillon - CC 72, CNRS/IN2P3, F-34095 Montpellier, France}
\address{$^{4}$ The Oskar Klein Centre for Cosmoparticle Physics and Department of Physics, Stockholm University, AlbaNova, SE 106 91, Stockholm, Sweden}
\ead{francesco.longo@ts.infn.it}

\begin{abstract} 
The \Fermi Large Area Telescope current generation experiment dedicated to gamma-ray astrophysics is massively using the CNAF resources to 
run its Monte-Carlo simulations through the Fermi-DIRAC interface on the grid under the virtual organization glast.org.
\end{abstract}

\section{The \Fermi LAT Experiment}

The Large Area Telescope (LAT) is the primary instrument on the \emph{Fermi Gamma-ray Space Telescope} mission% (formerly known as GLAST)
, launched on June 11, 2008.
It is the product of an international collaboration between DOE, NASA and academic US institutions as well as international partners in France,
Italy, Japan and Sweden.
The LAT is a pair-conversion detector of high-energy gamma rays covering the energy range from 20 MeV to more than 300 GeV \cite{LATinstrument}.
It has been designed to achieve a good position resolution ($<$10 arcmin) and an energy resolution of $\sim$10 \%. 
Thanks to its wide field of view ($\sim$2.4 sr at 1 GeV), the LAT has been routinely monitoring the gamma-ray sky and has shed light on the extreme, non-thermal Universe. This includes gamma-ray sources such as active galactic nuclei, gamma-ray bursts, galactic pulsars
and their environment, supernova remnants, solar flares, etc..

So far, the LAT has registered 400 billion trigger (1800 Hz average trigger rate).
An on-board filter analyses the event topology and discards about 80\%.
Of the 80 billion events that were transferred to ground 400 million were classified as photons.
All photon data are made public almost immediately.
Downlink, processing, preparation and storage take about 24 hours.

\section{Scientific Achievements Published in 2014}

In 2014, 54 collaboration papers (Cat.\ I and II) were published, keeping the pace of about 60 per year since launch.
Independent publications by LAT collaboration members (Cat.\ III) amount to 17.
Also external scientists are able to analyse the \Fermi public data, resulting in 192 external publications.

The major scientific achievements in 2014 were the observation of the extremely bright gamma-ray burst (GRB) 130427A \cite{Ackermann2014a,Preece2014}
(ref.\ \cite{Ackermann2014a} was chosen for the title page of the Science issue),
the observations in gamma-rays of four novae establishing them as a new class of gamma-ray sources \cite{Ackermann2014b} which triggered a NASA Press release \cite{NASA2014b},
and several papers on constraints for the annihilation of hypothetical dark matter particles \cite{Ackermann2014c,Drlica2014,Albert2014}.
Another discovery which triggered a NASA press release \cite{NASA2014a}
was the observation in gamma rays of the multiple images of a gravitationally lensed blazar \cite{Abdo2014a}.

\section{The Computing Model}

The \Fermi-LAT offline processing system is hosted by the LAT ISOC (Instrument Science Operations Center)
based at the SLAC National Accelerator Laboratory in California.
The \Fermi-LAT data processing pipeline (e.g.\ see \cite{LATp2} for a detailed technical description) 
was designed with the focus on allowing the management of arbitrarily complex work flows and handling multiple tasks simultaneously 
(e.g., prompt data processing, data reprocessing, MC production, and science analysis).
The available resources are used for specific tasks: the SLAC batch farm for data processing, high level analysis, and smaller MC tasks,
the batch farm of the CC-IN2P3 at Lyon and the grid resources for large MC campaigns.
The grid resources \cite{CHEP2013}
are accessed through a DIRAC (Distributed Infrastructure with Remote Agent Control) \cite{DIRAC} interface to the LAT data pipeline \cite{LAT-DIRAC}.
This setup was extensively tested in 2013 and the first months of 2014 and is in ordinary production mode since april 2014.

The jobs submitted through DIRAC constitute a substantial fraction of the submitted jobs.
However, we also exploit the possibility to submit jobs directly using the grid middleware.
Figure \ref{slots} shows the usage of grid resources in 2014.
About 22\% of the jobs were run at the INFN Tier 1 at CNAF as shown by Fig. \ref{piechart}.
The total usage in 2014 was 2969 HS06.
Assuming 10 HS06 per core, this is equivalent to about 300 CPU-years.

\begin{figure}[b]
\centering
\includegraphics[scale=0.41]{time.png}
\caption{Usage of grid sites by the VO glast.org in 2014}
\label{slots}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.45]{share.png}
\caption{Cumulative usage (in HEP-SPEC06) of grid sites by the VO glast.org in 2014}
\label{piechart}
\end{figure}


\section{Conclusions and Perspectives}
\label{conclusions}

The prototype setup based on the DIRAC framework described in the INFN-CNAF Annual Report 2013 \cite{CNAF2013} proved to be successful.
In 2014 we transitioned into production mode, and several large MC tasks were run using grid resources at the INFN-CNAF Tier 1 and elsewhere.
In 2015 we anticipate an increased demand for computing power in view of the massive simulation of ``Pass 8'' \cite{LATpass8} backgrounds now scheduled for summer 2015.

\section*{References}
\begin{thebibliography}{99}
\bibitem{LATinstrument} Atwood W B et al.\ 2009 {\it The Astrophysical Journal} {\bf 697} 1071
\bibitem{Ackermann2014a}%Fermi-LAT Observations of the Gamma-Ray Burst GRB 130427A
Ackermann M et al.\ 2014 {\it Science} {\bf 343} 42
\bibitem{Preece2014}%The First Pulse of the Extremely Bright GRB 130427A: A Test Lab for Synchrotron Shocks
Preece R et al.\ 2014 {\it Science} {\bf 343} 51
\bibitem{Ackermann2014b}%Fermi Establishes Classical Novae as a Distinct Class of Gamma-Ray Sources
Ackermann M et al.\ 2014 {\it Science} {\bf 345} 554
\bibitem{NASA2014b} NASA Press release 14-209 2014 July 31 http://www.nasa.gov/press/2014/july/nasas-fermi-space-telescope-reveals-new-source-of-gamma-rays/
\bibitem{Ackermann2014c}%Dark Matter Constraints from Observations of 25 Milky Way Satellite Galaxies with the Fermi Large Area Telescope
Ackermann M et al.\ 2014 {\it Phys.\ Rev.\ D} {\bf 89} 042001
\bibitem{Drlica2014}%Searching for Dark Matter Annihilation in the Smith High-Velocity Cloud
Drlica-Wagner A et al.\ 2014 {\it ApJ} {\bf 790} 24
\bibitem{Albert2014}%Search for 100 MeV to 10 GeV γ-ray lines in the Fermi-LAT data and implications for gravitino dark matter in the munuSSM
Albert A et al.\ 2014 {\it JCAP} {\bf 10} 023
\bibitem{NASA2014a}NASA Press release 14-005 2014 January 6 http://www.nasa.gov/press/2014/january/nasas-fermi-makes-first-gamma-ray-study-of-a-gravitational-lens/
\bibitem{Abdo2014a}%Gamma-ray flaring activity from the gravitationally lensed blazar PKS 1830-211 observed by Fermi LAT
Abdo A A et al.\ 2015 {\it ApJ} {\bf 799} 143
%\bibitem{Thompson2013} Thompson D J 2013 arXiv:1308.1870 
%%\bibitem{LAT2FGL} Nolan P L et al 2012 {\it The Astrophysical Journal Supplement Series} {\bf 199} 31
%\bibitem{LATpass7} Ackermann M et al.\ 2012 {\it The Astrophysical Journal Supplement Series} {\bf 203} 4
%\bibitem{LATdiffuse} Abdo A A et al.\ 2010 {\it Physical Review Letters} {\bf 104} 101101
%\bibitem{LATe+e-} Ackermann M et al.\ 2012 {\it Physical Review Letters} {\bf 108} 011103
%\bibitem{Gleam} Boinee P et al 2003 {\it Proceedings of the first workshop on Science with the new generation of high energy gamma-ray experiments : between astrophysics and astroparticle physics.}.\ Edited by S.\ Ciprini, A.\ De Angelis, P.\ Lubrano, and O.\ Mansutti,  141
\bibitem{LATp2} Dubois R 2009 {\it ASP Conference Series} {\bf 411} 189
\bibitem{CHEP2013} Arrabito L et al.\ 2013 CHEP 2013 conference proceedings arXiv:1403.7221
\bibitem{DIRAC} Tsaregorodtsev A et al.\ 2008 {\it Journal of Physics: Conference Series} {\bf 119} 062048
\bibitem{LAT-DIRAC} Zimmer S et al.\ 2012 {\it Journal of Physics: Conference Series} {\bf 396} 032121
\bibitem{CNAF2013} Arrabito L et al.\ 2014 {\it INFN-CNAF Annual Report 2013}, edited by L.\ dell'Agnello, F.\ Giacomini, and C.\ Grandi, pp.\ 46
\bibitem{LATpass8} Atwood W B et al.\ 2013 {\it 2012 Fermi Symposium: eConf Proceedings C121028} arXiv:1303.3514

\end{thebibliography}

\end{document}
