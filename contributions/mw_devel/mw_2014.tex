\documentclass[a4paper]{jpconf}
\usepackage{url}

\begin{document}
\title{Middleware support, maintenance and development}

\author{
  A. Ceccanti,
  D. Andreotti,
  E. Vianello,
  G. Dalla Torre,
  F. Giacomini
}

\address{INFN-CNAF, Bologna, Italy}

\ead{
  andrea.ceccanti@cnaf.infn.it,
  daniele.andreotti@cnaf.infn.it,
  enrico.vianello@cnaf.infn.it,
  gianni.dalla.torre@cnaf.infn.it,
  francesco.giacomini@cnaf.infn.it
}

\begin{abstract}

INFN-CNAF plays a major role in the support, maintenance and development
activities of key middleware components (VOMS, StoRM, Argus PAP) widely used in
the WLCG and EGI computing infrastructures. In this report, we discuss the main
activities performed in 2014 by the CNAF middleware development team.

\end{abstract}

\section{Introduction}

The CNAF middleware development team  has focused, in 2014, on the support,
maintenance and evolution of the following products:

\begin{itemize}

  \item VOMS~\cite{voms}: the attribute authority, administration
    server, APIs and client utilities which form the core of the Grid
    middleware authorization stack;

  \item StoRM~\cite{storm}: the lightweight storage element in
    production at the CNAF Tier1 and in several other WLCG sites;

  \item Argus Policy Administration Point (PAP)~\cite{argus}: the
    Argus administrative interface and policy repository.

\end{itemize}

The main activities for the year centered around support and
maintenance of the software and on the improvement of the continuous
integration and testing processes.

\section{The software development and maintenance process}

The software development, maintenance and evolution activities for VOMS, StoRM
and Argus follow the same process, and are driven by user requirements and
support requests that identify problems or shortcomings in the code.

All Requests for changes (RFCs) are tracked in the INFN JIRA
tracker~\cite{jira, jira-storm,jira-voms, jira-argus}, prioritised, and linked
to development sprints that are usually three weeks long. A development
sprint leads typically (but not always) to a software release which is
announced via the product website~\cite{voms-news} and announcement
mailing lists.

\subsection{Continuous Integration and testing}

All the code code for VOMS, StoRM and the Argus PAP is hosted on
Github~\cite{github}. The software is continuously built and tested by our
Jenkins~\cite{jenkins} server, which is configured with build nodes for the
main supported platforms (Scientific Linux 5 and 6).

The Github Webhooks~\cite{github-hooks} mechanism provides efficient
integration between the code repository and our CI server, so that whenever a
change is pushed to one of the managed repositories a new build job is started
on our CI infrastructure.

Continuous deployment tests for VOMS and StoRM run nightly, in order to
check that the latest versions of our products install and run correctly on all
supported platforms. The clean environment required for the deployment tests is
provided by the integration with the Cloud@CNAF local private cloud
infrastructure based on OpenStack~\cite{openstack} Havana.

\section{VOMS}

During 2014, 68 new issues were opened in the VOMS issue
tracker~\cite{jira-voms} to track maintenance, development and release
activities. In the same period, 74 issues were resolved.

The main highlights for VOMS are:

\begin{itemize}
  \item The release of VOMS Admin server version 3.3.0 and
    3.3.1~\cite{voms-admin-3.3.0, voms-admin-3.3.1}, providing several
    improvements and bug fixes mainly targeted at the VOMS deployment at CERN
    which serves the main LHC experiments
  \item The release of VOMS server version 2.0.12~\cite{voms-2.0.12}, to fix several issues
    found in production
  \item Minor fixes on the VOMS Java APIs~\cite{voms-api-java-3.0.5} and clients~\cite{voms-clients-3.0.6}
  \item Evolution of the VOMS functional and regression testsuite~\cite{voms-testsuite}
\end{itemize}

\section{StoRM}

During 2014, 85 new issues were opened in the StoRM issue
tracker~\cite{jira-storm} to track maintenance, development and release
activities. In the same period, 86 issues were resolved.

The main highlights for StoRM are:

\begin{itemize}
  \item The development of the StoRM WebDAV service, officially released after
    an initial testing phase in february 2015~\cite{storm-webdav-1.0.2}, to
    replace the StoRM GridHTTPs providing a more scalable webdav solution for
    StoRM
  \item The StoRM 1.11.4 and 1.11.5 releases~\cite{storm-1.11.4, storm-1.11.5},
    providing fixes for several issues found in production and during
    development
  \item Evolution of the StoRM functional and regression testsuite~\cite{storm-testsuite}
  \item Evolution of the load testsuite~\cite{load-testsuite}
\end{itemize}

\section{Argus}

In 2014, work on the Argus PAP focused on the support activities. No new
releases of the Argus PAP were issued.

\section{Future work}

Besides ordinary support and maintenance, in the future we will focus on
the following activities:

\begin{itemize}

  \item Refactoring of the StoRM frontend and backend services, to reduce
    code-base size and maintenance costs, and to provide horizontal scalability
    and simplifiy the services management;

  \item Evolution of the VOMS attribute authority for better integration with
    SAML federations;

  \item Continuous integration and delivery, by leveraging lightweight
    virtualization environments (e.g., Docker) for integration testing and
    simplified deployment in production.

\end{itemize}

\section*{References}
\begin{thebibliography}{99}

  \bibitem{egi} European grid Infrastructure \url{http://www.egi.eu}
  \bibitem{wlcg} The Worldwide LHC computing Grid \url{http://wlcg.web.cern.ch}
  \bibitem{voms} The VOMS website \url{http://italiangrid.github.io/voms}
  \bibitem{voms-news} The VOMS website news section\url{http://italiangrid.github.io/voms/news}
  %\bibitem{voms-clients} VOMS clients code repository \url{https://github.com/italiangrid/voms-clients}
  %\bibitem{voms-api-java} The VOMS Java APIs \url{https://github.com/italiangrid/voms-api-java}
  \bibitem{argus} Argus authorization service website \url{http://argus-authz.github.io}
  \bibitem{storm} StoRM website \url{http://italiangrid.github.io/storm}
  \bibitem{jenkins} Jenkins \url{https://jenkins-ci.org/}
  \bibitem{github} GitHub \url{https://github.com/}
  \bibitem{openstack} Openstack \url{http://www.openstack.org}
  \bibitem{jira} INFN issue tracker \url{https://issues.infn.it}
  \bibitem{jira-storm} StoRM on INFN JIRA \url{https://issues.infn.it/jira/browse/STOR}
  \bibitem{jira-voms} VOMS on INFN JIRA \url{https://issues.infn.it/jira/browse/VOMS}
  \bibitem{jira-argus} Argus on INFN JIRA \url{https://issues.infn.it/jira/browse/ARGUS}
  \bibitem{emi} The European Middleware Initiative \url{http://www.eu-emi.eu}
  \bibitem{voms-testsuite} The VOMS clients testsuite \url{https://github.com/italiangrid/voms-testsuite}
  \bibitem{storm-testsuite} The StoRM testsuite \url{https://github.com/italiangrid/storm-testsuite}
  \bibitem{load-testsuite} The StoRM load testsuite \url{https://github.com/italiangrid/grinder-load-testsuite}
  \bibitem{github-hooks} Github webhooks \url{https://help.github.com/articles/about-webhooks}
  \bibitem{github-pages} Github pages \url{https://pages.github.com}
  \bibitem{voms-admin-3.3.0} VOMS Admin 3.3.0 \url{http://italiangrid.github.io/voms/release-notes/voms-admin-server/3.3.0/}
  \bibitem{voms-admin-3.3.1} VOMS Admin 3.3.1 \url{http://italiangrid.github.io/voms/release-notes/voms-admin-server/3.3.1/}
  \bibitem{voms-2.0.12} VOMS 2.0.12 \url{http://italiangrid.github.io/voms/release-notes/voms-server/2.0.12/}
  \bibitem{voms-clients-3.0.6} VOMS clients 3.0.6 \url{http://italiangrid.github.io/voms/release-notes/voms-clients/3.0.6/}
  \bibitem{voms-api-java-3.0.5} VOMS Java APIs 3.0.5 \url{http://italiangrid.github.io/voms/release-notes/voms-api-java/3.0.5/}
  \bibitem{storm-1.11.4} StoRM v. 1.11.4 \url{http://italiangrid.github.io/storm/2014/04/23/storm-v.1.11.4-released.html}
  \bibitem{storm-1.11.5} StoRM v. 1.11.5 \url{http://italiangrid.github.io/storm/2015/01/07/storm-v.1.11.5-released.html}
  \bibitem{storm-webdav-1.0.2} StoRM WebDAV v. 1.0.2 \url{http://italiangrid.github.io/storm/release-notes/storm-webdav/1.0.2/}

\end{thebibliography}
\end{document}
