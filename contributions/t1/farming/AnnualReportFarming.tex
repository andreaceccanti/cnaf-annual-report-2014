\documentclass[a4paper]{jpconf}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{hyperref}
%%%%%%%%%% Start TeXmacs macros
\newcommand{\tmem}[1]{{\em #1\/}}
\newcommand{\tmop}[1]{\ensuremath{\operatorname{#1}}}
\newcommand{\tmtextit}[1]{{\itshape{#1}}}
\newcommand{\tmtt}[1]{\texttt{#1}}
%%%%%%%%%% End TeXmacs macros

\begin{document}
\title{The INFN Tier-1: the computing farm}

\author{Andrea Chierici}
\ead{andrea.chierici@cnaf.infn.it}

\author{Stefano Dal Pra}
\ead{stefano.dalpra@cnaf.infn.it}

\author{Giuseppe Misurelli}
\ead{giuseppe.misurelli@cnaf.infn.it}

\author{Saverio Virgilio}
\ead{saverio.virgilio@cnaf.infn.it}

%\begin{abstract}
%\end{abstract}

\section{Introduction}
The farming group is responsible for the management of the computing resources of the centre (including grid interfaces, CE and site BDII). This implies the deployment of installation and configuration services, monitoring facilities and to fairly distribute the resources to the experiments that have agreed to run at CNAF. 


%\begin{figure}
%\centering
%\includegraphics[keepaspectratio,width=10cm]{ge_functional.pdf}
%\caption{Host types and daemons}
%\label{ge_func}
%\end{figure}

%\begin{figure}
%\centering
%\includegraphics[keepaspectratio,width=10cm]{ge_arch.pdf}
%\caption{Grid Engine instance at INFN-T1}
%\label{ge_arch}
%\end{figure}


\section{Farming status update}
During 2014 the farm has been quite stable. We installed the acquired tender and dismissed some very old nodes, reaching and even exceeding our pledges: the computing power provided to experiments is roughly 160K HS06. Figure~\ref{job_trend} depicts the job submission trend during the first 9 months of the year.

\begin{figure}
\centering
\includegraphics[keepaspectratio,width=10cm]{images/job_trend.png}
\caption{Job submission trend}
\label{job_trend}
\end{figure}

To ease the deployment of virtual machines the virtualization infrastructure has been migrated to the ovirt software platform. Thanks to this solution farming group is able to delegate machine management to single users and to interface the infrastructure with other software tools (like the Foreman, a solution under investigation at CNAF in order to take over the old configuration and installation system).

\section{Low power CPUs investigation}
INFN-T1 is quite expensive to maintain, due the cost of electricity. We started to investigate some promising low power solutions developed by chip makers, trying to understand if they could fit our requirements of both computing power and cost per watt.

\subsection{Intel Avoton}
Our computing room PUE\cite{ref:pue} is 1,6 and the average monthly power required hits 1100kW. Our farm hosts several old nodes with a poor hs06/w ratio, hence we felt the need to better investigate solutions to improve power efficiency.
Recently Intel introduced the Avoton\cite{ref:avoton} processor (C2000 family) a system-on-chip built on 22 nanometer process technology. This chip comes with up to 8 64-bit cores based on Silvermont micro architecture, originally conceived to address the needs of microserver, entry level communication infrastructure and cloud storage markets. It's remarkable power consumption of 20W pushed us to investigate the possibility to use this chip in our farm as opposed to our standard server chip solutions (to give an idea, a common processor in our farm requires 115W).

\subsection{Tests}
\begin{figure}
\centering
\includegraphics[keepaspectratio,width=10cm]{images/c2750.png}
\caption{C2750 HS06 Calculation}
\label{c2750}
\end{figure}

\begin{figure}
\centering
\includegraphics[keepaspectratio,width=10cm]{images/hs06_per_tender.png}
\caption{HS06 per tender}
\label{hs06_per_tender}
\end{figure}

\begin{figure}
\centering
\includegraphics[keepaspectratio,width=10cm]{images/hs06w.png}
\caption{HS06 per watt}
\label{hs06w}
\end{figure}

We were able to test 2 different solutions, one based on a stand-alone appliance made by Supermicro, and the other based on the \textit{Moonshot} system by HP\cite{ref:moonshot}. In both cases the chip was the same and we obtained comparable results: the main difference is the form factor of the 2 solutions, one more targeted at stand-alone appliances, while the other to high density low storage solutions.
Figure~\ref{c2750} shows a good increase in HS06 according to the number of concurrent runs, meaning that the CPU is capable of scaling well till we reach the number of physical cores (8 in this CPU). More important is to compare this result with other CPUs in our computing farm, as depicted in figure~\ref{hs06_per_tender}. The Avoton CPU performs significantly slower than the others, less than half. This result is overturned if we evaluate the number of HS06 per Watt consumed by the CPUs: in this case, as depicted in figure~\ref{hs06w}, Avoton CPU greatly outperforms all the other solutions currently installed in our computing farm.

\subsection{TCO simulation}
To better understand the possibility to switch to this type of solution, we calculated a TCO simulation comparing the declared street price of a HP Moonshot system with the last CPU tender we acquired: the results are shown in table~\ref{tab:avoton_tco}.

\begin{table}

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
\textit{60k HS06} & \textit{last tender} & \textit{HP Moonshot} \\
\hline
n. Enclosures & 87 &25 \\
\hline
n. Racks & 6 & 4 \\
\hline
n.Motherboards & 348 & 1125 \\
\hline
kW required & 88 & 35 \\
\hline
w/HS06 & 1,47 & 0,56 \\
\hline
Purchase Cost & x & 1,68*x \\
\hline
Electricity Cost/Y & x & 0,4*x \\
\hline
\end{tabular}
\caption{TCO Simulation}
\label{tab:avoton_tco}
\end{center}
\end{table}

\begin{figure}
\centering
\includegraphics[keepaspectratio,width=10cm]{images/avoton_tco.png}
\caption{TCO simulation: Avoton CPU vs last tender}
\label{avoton_tco}
\end{figure}
 
From the table we can understand that the initial cost of the Avoton solution is significantly higher compared to the last tender, but we have to stress the fact that it was computed on street price, not on a tender price (where prices generally are significantly lower). Anyway starting from the fourth year (as depicted in figure~\ref{avoton_tco}) the solution becomes cost-effective.

\subsection{Conclusions}
Since the Avoton CPU uses the same microcode architecture of server chips, switching to this kind of solution is rather easy and simple, indeed no code recompilation is required (as compared to other low power solutions, like arm chips). The CPU TDP of only 20W means less cooling power is required to cool the computing room and this implies the possibility to save quite a lot of money. 
This solution apparently has got some drawbacks that need further investigation: even if the results are promising, the Avoton CPU is not so powerful compared to standard server class CPU, meaning that more computers have to be purchased in order to reach the pledged resources experiments require. More computers mean bigger human effort to keep all the nodes up and running and possibly more hardware failures. It's not easy to understand and decide if this solution is the best for our computing requirements, we will continue the investigation, possibly with the collaboration of hardware vendors, asking the possibility to deploy a certain number of such computers in our farm in order to test them in a real production environment.

\section{Emergency shut-down procedure}
INFN-T1 suffered a major cooling problem during 2014 and this pushed us to better implement emergency shut-down procedures for the computing room. If a cooling problem occurs it's vital to identify the nodes that can be easily switched off in order to keep the room temperature at an acceptable level: the bigger part of machines in the computing room is constituted by worker nodes, who can be easily switched off in case of emergencies, leaving time to other, more complicated infrastructures (like storage), to be taken care of (and eventually be switched off too).
Farming department implemented a procedure to smoothly switch off all the computing nodes, using both a software approach (a ``poweroff'' command issued via \texttt{ssh} command) and a hardware one (IPMI\cite{ref:ipmi} command to cut the power of the node). This procedure must be used with great caution and only in case of clear and present danger.
In the future this procedure could be made available to the on-call operator allowing us to increase security and safety of the computing room.

\section{Adapting a custom accounting system to APEL}

\subsection{Introduction}
Starting from October 2013, we upgraded the lower layer of the accounting system of the Tier--1 
with a solution designed and implemented from scratch, due to an incident occurred with the previous system, the Distributed Grid Accounting System (DGAS). Now, accounting
data for Grid and local jobs are stored in a \tmtt{acct} PostgreSQL database
whose content and schema is managed by the farming staff\cite{DGAS}.

Data propagation to the {\url{accounting.egi.eu}} portal is performed as
before, through the DGAS--HLR hierarchy: the MySQL database in the
first--level HLR is updated with the newest accounting records. This step,
formerly performed by specific DGAS components, has been replaced by a custom
component whose main task is to fetch new records from the
{\tmtt{acct}} database and to insert them into the {\tmtt{hlr}} database hosted in the site HLR.
These data were then transmitted to the second--level HLR, hosted and managed
by staff at INFN--Torino. This system acted as a central collector for all the
accounting data from all the sites in the italian Regional Operation Center
(ROC) and was also responsible of their delivering to the
{\url{accounting.egi.eu}} portal of WLCG accounting, managed by APEL\cite{APEL}.

On September 2014 the second--level HLR of the Italian ROC had to be
dismissed and after a short discussion, \ the
IGI consortium decided that Italian sites had to replace their DGAS
infrastructure with the one provided by APEL: the main difference is
that each APEL instance directly delivers usage records straight to the
{\url{accounting.egi.eu}} portal.

\subsection{Incompatibility issues}
A few issues prevented us from directly adopting the APEL accounting model and
forced us to study a different solution:
\begin{description}
  \item[multi--site batch system] Three Grid sites are supported by the 
  ``Tier--1'' computing centre, and they all rely on a single
  instance of the Platform/LSF batch system deployed on the farm: APEL however cannot fairly
  handle this case, as it assumes that each Grid site relies on its own batch system.
  
  \item[LSF log parsing] Even in the most recent APEL release available, 
  the component responsible of parsing the raw accounting records logged
  by LSF, proved to be bugged, failing to recognize a number of valid
  accounting records.
  
  \item[Wrong WallClockTime] LSF logs the time each job spends in {\tmtt{RUN}} status in
  the {\tmtt{runtime}} field. APEL however does not collect that value and
  sets {\tmtt{WallDuration}} as ``{\tmtt{endtime - starttime}}''. This is
  correct if the job is not suspended during its lifetime and if it is
  dispatched only once, which is not always guaranteed. Furthermore, if a job
  is killed before being started, the {\tmtt{starttime}} field of its log
  record remains at its default value, which is zero. This fools the APEL
  parser to account for a job with a {\tmtt{WallDuration}} equal to
  \tmtextit{epoch}, i.e. as if it started in 01-01-1970.
  
  \item[multi--core] The number of used cores is not considered in the
  computation of {\tmtt{WallDuration}} for multi--core jobs, which then turns
  out to have an efficiency $E = \frac{CPUtime}{WCtime} > 1$.
  This is not consistent with the case of single--core jobs and should instead be
  computed as $E = \frac{CPUtime}{cores \times WCtime}$.
  
  \item[HPC] INFN--T1 hosts a small HPC cluster, managed by LSF 9.1, running
  many--core, multi--core or GPU based applications. The accounting of the
  activity of this cluster is done using the same custom accounting system
  described in \cite{DGAS}. This enables us to quickly adapt our accounting
  requirements even on new and different use-cases, such as that of GPU computing.
  
  \item[Custom needs] A generic accounting system should aim at collecting and
  storing only those accounting information that make sense on the widest set
  of batch systems: still, many useful specific data are logged by the LSF
  batch system, and these are valuable to the farm staff (such as the queue name,
  the submission or execution host, the job exit status, the submission
  request parameters, etc.). Thus, the overall information gathered by our local
  accounting is a superset of those currently required and requested to provide
  accounting information. A single accounting system (with all the data of interest stored in it) is simpler to maintain and eases the task of extracting a specific subset of info to fulfil a particular request.
\end{description}

\subsection{Switching the data propagation model}
After an incident happened at the 2$^{nd}$ level HLR and its subsequent dismissal, 
the way to propagate the accounting data from the Tier--1 to the EGI portal had to be changed. 
The data delivered from our local {\tmtt{acct}} database had to be
adapted to directly transmit accounting records to the EGI--APEL database
using the {\tmtt{ssmsend}} utility coming from the APEL distribution.

\subsubsection{The APEL patching attempt}
At first, an attempt to adapt APEL software to solve the aforementioned issues
was made. Three APEL python programs were patched to provide multi--site
awareness and multi--core compliance. Furthermore, the low level parsing of
the raw LSF log files was adapted to use a reliable open source library,
from the {\tmtt{python-lsf-tools}} collection. The former log parsing approach 
used the {\tmtt{lsf.py}} low level module from the APEL distribution, that is based on a regular
expression which simply is too fragile and fails to correctly parse the records
on a certain number of corner cases. The {\tmtt{accounting.py}} library from
{\tmtt{python-lsf-tools}} proved indeedto be more reliable.

After verifying the updates were correct, a ticket (\#109485) had been submitted to {\url{https://ggus.eu}} to propose the APEL team the adoption of the patches and to get support during the process of the
adaptation of the accounting data delivery.

\subsubsection{The working approach}
Even if this approach proved to produce correct data, while waiting for the feedback about the proposed patches, we followed a different strategy:
the data to be delivered using the {\tmtt{ssmsend}} APEL utility from the
{\tmtt{apel-ssm}} package are produced by a custom script which extracts
them from the {\tmtt{acct}} database and then dumps them into a set of files
equivalent to those produced by the original APEL system. Each file contains
one--thousand usage records and is deleted just after its delivery. 
The final outcome of the implemented solution has a significant
difference with respect to what would be done using current APEL approach: the {\tmtt{Site}}
field doesn't come any more from a configuration file, it hasn't static values hard coded in it but
is defined instead as a map of the submission host. This enables one single host to deliver all the accounting records produced by a Batch System instance, despite the number of CEs used or the logical Grid Sites relying on it.

\subsubsection{Faust}
Short after dismissing the old 2$^{nd}$ level HLR, the former DGAS staff
introduced \tmtextit{Faust}, a new accounting repository for the Italian ROC,
offering a web--service based API interface intended for extracting graphical
reports for the client. This repository has to be fed with the
same data delivered to APEL, so the transmission process is repeated
twice, simply providing a different configuration file to the {\tmtt{ssmsend}}
utility.

\subsubsection{Validation}
The validation process for the new system was followed through the
aforementioned ggus ticket, which provides a pretty detailed report
on the progress of this activity.

\subsection{Conclusions}
Our implementation proved to be reliable enough to be used in every day production: we have now a more robust accounting system that is used every day by a great number of people flawlessly.

\section{Dynamic partitioning for multi--core and high--memory provisioning with LSF}
  During January 2014, The INFN--T1 was requested to enable the
  execution of multi--core applications, using 8 cores simultaneously in
  the same Worker Node. Even thought current batch systems are
  commonly able to accept and manage a great variety of jobs, this
  kind of activity requires special configurations in a typical WLCG
  farm: this is because the common workload is given by single--core
  jobs. A 8--core job can only start when eight free
  slots are available at the same time in the same node, which is an
  extremely unlikely event.

\subsection{Traditional solutions and their drawbacks}
  \begin{description}
  \item[Host partitioning:] this is the most simple and
    straightforward solution. A fixed subset of the Worker Nodes of
    the site are exclusively dedicated to multi--core only
    activity. This is unacceptable at the INFN--T1, because too many
    resources would remain unused when not enough multi--core processing
    is in progress or because subset may bee undersized during a
    boosted multi--core demand.
    
   \item[Advanced reservation:] this is a standard feature provided by
     most batch systems, LSF included. Assuming that the batch system
     knows the maximum expected end--time for each running job, it can
     select the node where enough slots are early going to be
     free. The node can be exclusively reserved for a specific
     multi--core job during a suitable time window, when currently
     running jobs are expected to be finished.

     The duration of most of the running jobs is totally uncertain,
     varying from a few seconds (extremely frequent with {\em empty
       pilot} jobs) to a large upper run limit defined at queue level.
     This means that each time a node gets reserved, a {\em drain time}
     begins, during which no single--core jobs can be scheduled on it,
     causing free slots to be unusable until enough are available to host
     a multi--core job. Furthermore, the batch system would
     systematically trigger an advanced reservation for each pending
     multi--core job, adding more and more CPU power loss due to
     draining times.
  \end{description}

  The available known methods to run a mixture of single and multi core
  jobs are not general enough to fulfil our case, so a
  different model had to be designed.

\subsection{The dynamic partitioning model}
   A desired feature would consist in having a varying number of nodes
   dedicated to multi--core according on needs. This would be equivalent
   to an auto resizing host partition, and can be defined according to
   the following principles:

   \begin{description}
      \item[Drain one, run many:] once selected for multi--core and put
        on draining, a node should run multi--core jobs as long as
        possible, in order to minimize the impact on the drain time.
     
      \item[Follow the demand:] depending on the number of pending
        multi--core jobs, more nodes should be dedicated, up to a
        defined threshold.
     
      \item[Avoid emptiness:] if no multi--core jobs are being scheduled
        to a dedicated node, it should be reverted back to work with
        ordinary single core activity.
   \end{description}

\subsection{Implementation with LSF}
   The dynamic partitioning has been implemented by a few python
   scripts, a couple of simple C programs and a configuration
   file. The main elements are:
   \begin{description}
     \item[elim:] to obtain a dynamic partition, worker nodes are tagged with a {\tt
      mcore} flag. WNs whose flags equal one are dedicated to
    multi--core. This means that:
    \begin{itemize}
       \item single--core jobs cannot be dispatched to nodes having {\tt mcore==1}
       \item multi--core jobs must be dispatched to nodes having {\tt mcore==1}
    \end{itemize}
    The flag is, in LSF terminology, an {\em external load index} and
    its value is computed and reported by the WN itself, by running an
    {\em elim} script written by the administrator. This computes and
    prints to its {\tt stdout} the value of the flag every 60 seconds.

  \item[esub:] for each job, at submission time, another custom script,
    the external submitter {\em esub} is executed in the submission
    host (the CREAM--CE, in our case). It distinguishes
    multi--core jobs from single--core ones by inspecting the
    submission parameters, then modifying one of them, the {\em resource
      request} to ask {\tt mcore==1} for multi--core jobs and {\tt
      mcore!=1} for single--core jobs.

  \item[director:] a third script, the {\em director}, implements the
    logic of the partitioning model. It runs every six minutes and
    decides which nodes are to be added or removed from the mcore
    partition. The result is written in a Json file on a shared
    filesystem, accessible to any node in the LSF cluster. The {\em
      elim} running on each WN decides the value of its own mcore flag
    status by inspecting this file.

  \item[status:] the director makes use of status information, which are
    provided by a couple of simple {\tt C} programs based on the {\tt
      lsf/lsbatch.h} api. These information are:
    \begin{itemize}
    \item the resource request of each pending or running job.
    \item the current set of unavailable or closed nodes.
    \end{itemize}

  \item[configuration:] it is possible to tune the behaviour of the
    dynamic partitioning by configuring the host groups available for
    multi--core, specified as a list of racks in our case, the maximum
    number of nodes that can be in drain status, the maximum
    acceptable number of unused slots, the higher acceptable ratio
    of unused slots, and a number of other minor details.

  \item[hosts:] we selected for mcore queue, WNs with 16 and with 24 slots, thus being able to host two or    
    three mcore jobs.
\end{description}

   \begin{figure}
\begin{center}
      \begin{tikzpicture}[node distance=15mm]
        \tikzstyle{every node}=
        [%
          fill=green!50!black!20,%
          draw=green!50!black,%
          minimum size=7mm,%
          circle,%
          thick%
        ]

        \node (M) {$M:0$};
        \node (D) [right of=M] {$D:1$};
        \node (R) [below of=D] {$R:1$};
        \node (P) [below of=M] {$P:0$};

        %%\path [thick,shorten >=1pt,-stealth']
        \path [thick,shorten >=1pt]
                         (D) edge [loop above] (D)
                         (M) edge [loop above] (M)
                         (P) edge [loop below] (P)
                         (R) edge [loop below] (R)
                         (M) [->] edge (D)
                         (D) [<->] edge (P)
                         (D) [<->] edge (R)
                         (P) [->] edge (M)
;
      \end{tikzpicture}
\end{center}
\caption{The status transition map}
\label{fig:statemachine}
\end{figure}

\subsubsection{Transitions}
The director considers each node as a member of one of four disjoint sets:
\begin{itemize}
   \item $M$: available for mcore. This is initially
     the set of hosts defined in the configuration file. Only
     single--core jobs can be dispatched to nodes in this set, and they
     have their mcore flag set to 0.
   \item $D$: Assigned to mcore. A node in this set
     is in drain time. It may have single--core running jobs but no
     single--core can be dispatched there. It might have up to eight
     free slots. Nodes in this sets have their mcore flag set to 1.
   \item $R$: Running only multi--core jobs,
     drain time finished, no free slots. Nodes in this sets have their
     mcore flag set to 1.
   \item $P$: Purged from mcore. A node in this set comes from the $D$
     set, where it was found to have free room for multi--core jobs
     after many (default: three) consecutive checks. There are
     multi--core jobs still running, however single--core only can be
     dispatched to Nodes in this set. The mcore flag is set to 0.
\end{itemize}

\subsubsection{Dynamic partitioning}
The dynamic partitioning works like a finite state machine, as represented in Fig.~\ref{fig:statemachine}
\begin{enumerate}
\item At $T=0$, all WNs are $w_i$ in the set $M= \{w_1,\dots,w_N\}$
\item When $Q_m>0$ multi--core jobs are queued, $k$ WN are moved from
  $M$ to $D=\{w_1,\dots,w_k\}$ by the director.
\item When a node is full of multi--core, it is moved from $P$ to $R$.
\item When a node $w_i \in D$ has free room for a multi--core and no jobs
  have started there after a timeout, it is moved from $D$ to $P$.
\item When more multi--core nodes are required, they are moved from $P \cup M$ to $D$, beginning with $P$.
\item The elim script on each node $w_i$ updates its mcore status:

$$ mcore(w_i) = \left\{ \begin{array}{rl} 
1 & if\; w_i \in D \cup R\\
0 & if\; w_i \in M \cup P\\
\end{array}\right.
$$

\end{enumerate}

\begin{figure}[!htb]
  \minipage{0.49\textwidth}
  \begin{center}
  \includegraphics[width=1.0\textwidth]{images/mcore_cumulative.png}
  \caption{MCORE: Done jobs}\label{fig:mcore_cumul}
  \end{center}
  \endminipage\hfill
  \minipage{0.49\textwidth}
  \begin{center}
  \includegraphics[width=1.0\textwidth]{images/atlas_himem_cumulative.png}
  \caption{ATLAS HIMEM: Done Jobs}\label{fig:himem_cumul}
  \end{center}
  \endminipage\hfill
\end{figure}

\subsection{Deployment and improvements: high memory jobs}
  The dynamic partitioning system started working on the production
  batch system on the 1\textsuperscript{st} of August 2014, and proved to work successfully
  (Fig.~\ref{fig:mcore_no_himem}) without the need for manual
  interventions.

  During October, a new requirement came from the ATLAS LHC experiment
  to provide resources for the so called {\em himem} jobs. These are
  special single--core jobs requiring twice the RAM of an
  ordinary one.

  To fulfil this, himem jobs are treated like multi--core
  ones, except that they require two slots in the mcore
  partition. This way one core remains idle, however the working one has the
  requested amount of RAM. Furthermore, LSF is configured to run no more
  than four such jobs per node. Doing so, resources are always granted for
  at least two 8--core jobs on nodes with 24 slots.

  This solution has the benefit of reducing the number of unused
  slots, at the cost of a little delay in the start of 8--core
  jobs on the node.
  \begin{figure}[!htb]
    \minipage{0.49\textwidth}
\begin{center}
   \includegraphics[width=1.0\textwidth]{images/mcore_20days.png}
   \caption{Dynamic partition early days, August 2014,
     multi--core. The space between red and green line represents
     unused slots. Sudden submission dropdown have negative impact on
     average efficiency (see
     Fig.~\ref{fig:mcore_aug2014}). Different configurations have
     impact on the reactivity of the system, i.e. how quickly the
     partition grows or shrinks.}\label{fig:mcore_no_himem}
\end{center}
\endminipage\hfill
    \minipage{0.49\textwidth}
\begin{center}
        \includegraphics[width=1.0\textwidth]{images/mc20days_03_2015.png}
        \caption{Dynamic partition, March 2015, multi--core and
          high--memory. The number of unused slots over time is much
          reduced respect to the case of
          Fig.~\ref{fig:mcore_no_himem}. By inspecting the {\em used
            slots} line it can be noted how the fill factor decreases
          when there is lack of himem jobs. The partition reaches
          greater size and dropdown are less frequent.}\label{fig:mc20days2015}
  \end{center}
\endminipage\hfill
\end{figure}

\begin{figure}[!htb]
      \minipage{0.49\textwidth}
      \begin{center}
        \includegraphics[width=1.0\textwidth]{images/mcore_aug2014.png}
        \caption{Atlas, multi--core, 2014 Aug. The
          submission flow suddenly interrupts several times a month. As a
          consequence nodes in the mcore partition are put back at work
          with single--core jobs. Short after, submission flow restart,
          triggering the need for a new draining session.}\label{fig:mcore_aug2014}
      \end{center}
      \endminipage\hfill
      \minipage{0.49\textwidth}
      \begin{center}
        \includegraphics[width=1.0\textwidth]{images/mcore_mar2015b.png}
        \caption{Atlas, multi--core and high--memory, March 2015. The
          number of unused slots over time is greatly reduced respect
          to the case of Fig.~\ref{fig:mcore_aug2014}. By inspecting
          the {\em used slots} line it can be noted how the fill
          factor reduces when the himem job submission flow
          stops.}\label{fig:mc20days2015}
      \end{center}
      \endminipage\hfill
\end{figure}

%% SELECT
%% substr(date(from_unixtime(eventtimeepoch)) ::text,1,7) AS "Month",
%% numprocessors AS cores,
%% split_part(userfqan,'/',2) AS "VO",
%% COUNT(*) AS N,
%% (SUM(utime+stime+0.0)/86400)::NUMERIC(9,3) "CPT_days",
%% (SUM(runtime*numprocessors+0.0)/86400)::NUMERIC(9,3) "WCT_days",
%% (AVG((utime+stime+0.0)/(runtime*numprocessors))*100)::NUMERIC(9,3) AS "%Eff",
%% (AVG(utime+stime)/3600)::NUMERIC(9,3) AS "avg_CPT_h",
%% (AVG(runtime)*8/3600)::NUMERIC(9,3) AS "avg_WCT_h"
%% FROM gridjob
%% WHERE
%% eventtimeepoch >= to_unixtime('2015-03-01') -3600 AND
%% eventtimeepoch < to_unixtime('2015-04-01') -7200 AND
%% queue IN ('cms','cms_mcore','atlas','mcore','atlas_himem') AND
%% runtime > 180 AND
%%  split_part(userfqan,'/',2) IN ('atlas','cms') AND
%%  split_part(split_part(userfqan,'/',3),'=',2) IN ('pilot','production')
%% GROUP BY "Month","VO",cores
%% ORDER BY cores,"VO","Month";

\begin{center}
\begin{table}[h]
\begin{tabular}{l | r | l | r | r | r | r | r | r}
\textit{Month} & \textit{cpu} & \textit{VO} & \textit{n} &
\textit{CPT\_days} & \textit{WCT\_days} & \textit{\%Eff} &
\textit{$E[CPT]\,[h]$} & \textit{$E[WCT]\,[h]$} \\ \hline 2015-03 & 1
& atlas & 338737 & 57508.103 & 60272.685 & 67.217 & 4.075 & 34.163
\\ 2015-03 & 1 & cms & 207049 & 57664.471 & 74034.616 & 39.111 & 6.684
& 68.654 \\ 2015-03 & 2 & atlas & 52900 & 3529.259 & 7683.582 & 43.272
& 1.601 & 13.944 \\ 2015-03 & 8 & atlas & 23848 & 13799.668 &
18293.281 & 70.298 & 13.888 & 18.410 \\ 2015-03 & 8 & cms & 2798 &
8511.604 & 11948.709 & 28.167 & 73.009 & 102.491 \\
\end{tabular}
\caption{Activity by core and VO, March 2015. Noticeably, the average
  efficiency of multi--core jobs ($cpu=8$) is higher than that of
  single--core. This partially compensates the cpu power loss due to draining.}
\label{tab:acct_mar2015}
\end{table}
\end{center}

\subsection{Results}
The dynamic partitioning started in August 2014, enabling access to
compute resources for 8--core jobs. Two months later it was adapted to
enable high--memory jobs too. Fig.~\ref{fig:mcore_cumul} and
\ref{fig:himem_cumul} report cumulative CPUTime and WallclockTime for
multi--core and high--memory activity. Tab.~\ref{tab:acct_mar2015}
reports {\em per cpu} accounting data for March 2015.

In order to evaluate performances, a {\em Fill Factor} was considered,
defined as $FF=\frac{used\;slots}{dedicated\;slots}$, with optimal
value $FF=1$. Fig.~\ref{fig:mcore_no_himem} shows an insufficient
behaviour, with an average fill factor $FF=0.84$ and a partition size
of 335 slots, 65 of which unused. On the other hand, the partition
grows and shrinks itself according to the multi--core jobs submission
flow (Fig.~\ref{fig:mcore_aug2014}). As expected, during time periods of
steady submission flow, the fill factor increases near to 1.

Fig.~\ref{fig:mc20days2015} shows much better performances, with
$FF=0.95$ and an average partition size of 1466 slots, 71 of which
unused. The improvement is due to a more regular submission rate and
to high--memory jobs, running in the mcore partition with 2 assigned
slots each. 


\subsection{Conclusions}
A dynamic partitioning system for the LSF batch system has been
designed and implemented at INFN--T1. It works smoothly, provides
resources for multi--core and high--memory jobs. The system is more
efficient with smooth job submission flows and suffer with sudden
interruptions and restarts, because of the need for draining filled resources.
(Fig.~\ref{fig:mcore_aug2014}). High--memory jobs are helpful to
reduce the number of unused slots on the draining nodes. Having more
independent multicore submitters also helps in preventing partition
collapsing.


\section{Monitoring dashboard}
INFN-T1 monitoring and alarming agents produce tons of data describing state, performance and usage of our resources. Before we began this project, there was a lack in usage statistics reporting, a simple page where management board could look at how many jobs run during a certain period, how many HS06 they consumed as well as other kind of statistics showing how the data center is used by experiments. 
Hence, at the beginning of 2014, we started a cross-groups project aimed to collect, store, process and publish metric data centrally on a web portal (that we called Monviso): now we can provide both our resource administrators and user community with a central monitoring dashboard.

Every group inside INFN-T1 (like farming, storage and network) collect data and metric in a different way, due to the different devices used and solutions implemented during the years. It was not requested to drop every tool and start from scratch, but instead we focused on implementing a common set of rules to present the different data in a similar way. Every collector (tipically an INFN-T1 internal group) must follow these rules:
\begin{itemize}
\item sends their own data based on a common metric domain
\item metrics are stored in a time-series database
\item metric data should be consumed in a programmatic way
\end{itemize}

We chose to avoid the development of in house solutions in favour of open source ones, already adopted by large communities: we opted for the Graphite\cite{ref:graphite} project which provides a suite of components able to deal with the aforementioned requirements. 
In fact, it consists of three modules: 
\begin{description}
\item[Carbon component] \hfill \\
for dealing with incoming data saving them into the database 
\item[Whisper time-series]  \hfill \\
database library to implement the required round-robin-database like functionality\cite{ref:tsdb}
\item[Graphite-Web]  \hfill \\
application that renders graphs and dashboards
\end{description}

In order to collect data in the time-series database, before deploying Graphite and data producers we agreed on a metric domain with the rationale to distinguish each group by its prefix followed by the metric name of the type of data it carries on. For instance, \textit{farming.accounting} gives access to the farming group accounting data about executed jobs. In this scope, metrics are organized within the sub-domain \textit{experiment\_name.type\_of\_job.unit} (possible query about HS06 for all the experiment and grid jobs).

\begin{figure}
\centering
\includegraphics[keepaspectratio,width=10cm]{images/json-snippet.png}
\caption{Snippet of the json data returned by the Graphite url API}
\label{fig:json-snippet}
\end{figure} 
 
Consequently, we are now able to consume Graphite data-points based on the json format it provides by simply setting the proper url request parameters. 

For example, to compare the amount of cpu time consumed by the Alice experiments over the wall clock time we can just make the following http request:

\begin{verbatim}
http://graphite_fqdn/render?target= \\
asPercent(sumSeries(farming.mon.alice.*.cpt), \\
sumSeries(farming.mon.alice.*.wct))&from=-1h&format=json
\end{verbatim}

which results in the job efficiency (percentage of CPT/WCT) for all the Alice jobs (figure ~\ref{fig:json-snippet}).

\begin{figure}
\centering
\includegraphics[keepaspectratio,width=10cm]{images/homepage.png}
\caption{Monviso homepage}
\label{fig:monviso-homepage}
\end{figure}

The result of this work is the Monviso web portal, which handles \texttt{json} data-points and depicts them in charts. Its web structure is based on the Bootstrap\cite{ref:bootstrap} framework to deal with all the \texttt{html}, \texttt{css} and \texttt{js} stuff. Charts are generated by exploiting the Jqplot\cite{ref:jqplot} library plus a set of jQuery\cite{ref:jquery} and server side logic to get \texttt{json} data from Graphite and depict results in charts.

With the Monviso portal (figure~\ref{fig:monviso-homepage}) we have now a central point to gather resource usage reports on computing, storage, network and facility. 

%\begin{figure}
%\centering
%\includegraphics[keepaspectratio,width=10cm]{flat_vs_db.pdf}
%\caption{Flat files vs DB approach}
%\label{flat_vs_db}
%\end{figure}




\section{References}
\begin{thebibliography}{9}
\bibitem{ref:pue} PUE on Wikipedia: http://en.wikipedia.org/wiki/Power\_usage\_effectiveness
\bibitem{ref:avoton} Avoton on Intel Website: http://ark.intel.com/products/codename/54859/Avoton
\bibitem{ref:moonshot} HP Moonshot system Website: http://www8.hp.com/us/en/products/servers/moonshot/
\bibitem{ref:ipmi} IPMI on Wikipedia: http://en.wikipedia.org/wiki/Intelligent\_Platform\_Management\bibitem{DGAS} S. Dal Pra, ``Accounting Data Recovery. A Case Report from
  INFN-T1'' Nota interna, Commissione Calcolo e Reti dell'INFN,
  {\tt CCR-48/2014/P}
\bibitem{APEL} \url{https://wiki.egi.eu/wiki/APEL}
\bibitem{TF} \url{https://twiki.cern.ch/twiki/bin/view/LCG/DeployMultiCore}
\bibitem{JP} S. Dal Pra ``Job Packing: optimized configuration for job scheduling'', HEPiX Spring 2013 Workshop, \url{https://indico.cern.ch/event/220443/session/5/contribution/9}
\bibitem{LSF} LSF Admin guide \url{http://www-304.ibm.com/support/customercare/sas/f/plcomp/platformlsf.html}
\bibitem{ref:graphite} Graphite on Github repo: https://github.com/graphite-project
\bibitem{ref:tsdb} Time-serie database on Wikipedia: http://en.wikipedia.org/wiki/Time\_series\_database 
\bibitem{ref:bootstrap} Bootstrap website: http://getbootstrap.com 
\bibitem{ref:jqplot} JQplot website: http://www.jqplot.com
\bibitem{ref:jquery} JQuery website: http://jquery.com
\end{thebibliography}

\end{document}
